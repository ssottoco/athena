
L2muon:	HLT_mu24_idperf_L1MU14FCH:key=HLT_IDTrack_Muon_FTF:roi=HLT_Roi_L2SAMuon
L2muon:	HLT_mu26_ivarperf_L1MU14FCH:key=HLT_IDTrack_MuonIso_FTF:roi=HLT_Roi_MuonIso:post:d0=4
       
EFmuon:	HLT_mu24_idperf_L1MU14FCH:key=HLT_IDTrack_Muon_FTF:roi=HLT_Roi_L2SAMuon
EFmuon:	HLT_mu24_idperf_L1MU14FCH:key=HLT_IDTrack_Muon_IDTrig:roi=HLT_Roi_L2SAMuon
EFmuon:	HLT_mu26_ivarperf_L1MU14FCH:key=HLT_IDTrack_MuonIso_IDTrig:roi=HLT_Roi_MuonIso:post:d0=4
    

    
L2muonLowpt:	HLT_mu6_idperf_L1MU5VF:key=HLT_IDTrack_Muon_FTF:roi=HLT_Roi_L2SAMuon
L2muonLowpt:    HLT_mu4_mu4_idperf_1invmAB5_L12MU3VF:key=HLT_IDTrack_Muon_FTF:roi=HLT_L2SAMuon:te=1

EFmuonLowpt:	HLT_mu6_idperf_L1MU5VF:key=HLT_IDTrack_Muon_FTF:roi=HLT_Roi_L2SAMuon
EFmuonLowpt:	HLT_mu6_idperf_L1MU5VF:key=HLT_IDTrack_Muon_IDTrig:roi=HLT_Roi_L2SAMuon
EFmuonLowpt:	HLT_mu4_mu4_idperf_1invmAB5_L12MU3VF:key=HLT_IDTrack_Muon_IDTrig:roi=HLT_L2SAMuon:te=1
    

L2muonTnP:	HLT_mu24_idperf_L1MU14FCH:key=HLT_IDTrack_Muon_FTF:roi=HLT_Roi_L2SAMuon
L2muonTnP:	HLT_mu14_mu14_idtp_idZmumu_L12MU8F:key=HLT_IDTrack_Muon_FTF:roi=HLT_Roi_L2SAMuon:te=0:post:extra=mu0_tag
L2muonTnP:	HLT_mu14_mu14_idtp_idZmumu_L12MU8F:key=HLT_IDTrack_Muon_FTF:roi=HLT_Roi_L2SAMuon:te=1:post:extra=mu0_probe
        
EFmuonTnP:	HLT_mu24_idperf_L1MU14FCH:key=HLT_IDTrack_Muon_IDTrig:roi=HLT_Roi_L2SAMuon
EFmuonTnP:	HLT_mu14_mu14_idtp_idZmumu_L12MU8F:key=HLT_IDTrack_Muon_IDTrig:roi=HLT_Roi_L2SAMuon:te=0:post:extra=mu1_tag
EFmuonTnP:	HLT_mu14_mu14_idtp_idZmumu_L12MU8F:key=HLT_IDTrack_Muon_IDTrig:roi=HLT_Roi_L2SAMuon:te=1:post:extra=mu1_probe
    



L2muonLRT:	HLT_mu20_LRT_idperf_L1MU14FCH:key=HLT_IDTrack_MuonLRT_FTF:roi=HLT_Roi_L2SAMuon_LRT
L2muonLRT:	HLT_mu24_idperf_L1MU14FCH:key=HLT_IDTrack_Muon_FTF:roi=HLT_Roi_L2SAMuon
    
EFmuonLRT:	HLT_mu20_LRT_idperf_L1MU14FCH:key=HLT_IDTrack_MuonLRT_IDTrig:roi=HLT_Roi_L2SAMuon_LRT
EFmuonLRT:	HLT_mu24_idperf_L1MU14FCH:key=HLT_IDTrack_Muon_IDTrig:roi=HLT_Roi_L2SAMuon
        
    
L2muonLRTLowpt:	HLT_mu6_LRT_idperf_L1MU5VF:key=HLT_IDTrack_MuonLRT_FTF:roi=HLT_Roi_L2SAMuon_LRT
L2muonLRTLowpt:	HLT_mu6_idperf_L1MU5VF:key=HLT_IDTrack_Muon_FTF:roi=HLT_Roi_L2SAMuon 
        
EFmuonLRTLowpt:	HLT_mu6_LRT_idperf_L1MU5VF:key=HLT_IDTrack_MuonLRT_IDTrig:roi=HLT_Roi_L2SAMuon_LRT
EFmuonLRTLowpt:	HLT_mu6_idperf_L1MU5VF:key=HLT_IDTrack_IDTrack_Muon_IDTrig:roi=HLT_Roi_L2SAMuon
    
    

    
L2electron:	HLT_e26_idperf_loose_L1eEM26M:key=HLT_IDTrack_Electron_FTF:roi=HLT_Roi_FastElectron
L2electron:	HLT_e28_idperf_loose_L1eEM28M:key=HLT_IDTrack_Electron_FTF:roi=HLT_Roi_FastElectron
        
EFelectron:	HLT_e26_idperf_loose_L1eEM26M:key=HLT_IDTrack_Electron_FTF:roi=HLT_Roi_FastElectron
EFelectron:	HLT_e26_idperf_loose_L1eEM26M:key=HLT_IDTrack_Electron_IDTrig
EFelectron:	HLT_e28_idperf_loose_L1eEM28M:key=HLT_IDTrack_Electron_IDTrig
EFelectron:	HLT_e26_idperf_tight_L1eEM26M:key=HLT_IDTrack_Electron_GSF
    


L2electronLowpt: HLT_e5_idperf_tight_L1eEM5:key=HLT_IDTrack_Electron_FTF:roi=HLT_Roi_FastElectron
L2electronLowpt: HLT_e5_idperf_tight_nogsf_L1eEM5:key=HLT_IDTrack_Electron_FTF:roi=HLT_Roi_FastElectron
    
EFelectronLowpt: HLT_e5_idperf_tight_L1eEM5:key=HLT_IDTrack_Electron_FTF:roi=HLT_Roi_FastElectron
EFelectronLowpt: HLT_e5_idperf_tight_L1eEM5:key=HLT_IDTrack_Electron_IDTrig  	
EFelectronLowpt: HLT_e5_idperf_tight_L1eEM5:key=HLT_IDTrack_Electron_GSF 

    

L2electronTnP:	HLT_e26_idperf_loose_L1eEM26M:key=HLT_IDTrack_Electron_FTF:roi=HLT_Roi_FastElectron 
L2electronTnP:	HLT_e28_idperf_loose_L1eEM28M:key=HLT_IDTrack_Electron_FTF:roi=HLT_Roi_FastElectron
L2electronTnP:	HLT_e26_lhtight_e14_idperf_tight_nogsf_probe_50invmAB130_L1eEM26M:key=HLT_IDTrack_Electron_FTF:roi=HLT_Roi_FastElectron:te=0:post:extra=el_tag 
L2electronTnP:	HLT_e26_lhtight_e14_idperf_tight_nogsf_probe_50invmAB130_L1eEM26M:key=HLT_IDTrack_Electron_FTF:roi=HLT_Roi_FastElectron:te=1:post:extra=el_probe


EFelectronTnP:	HLT_e26_idperf_loose_L1eEM26M:key=HLT_IDTrack_Electron_GSF 
EFelectronTnP:	HLT_e26_lhtight_e14_idperf_tight_nogsf_probe_50invmAB130_L1eEM26M:key=HLT_IDTrack_Electron_FTF:roi=HLT_Roi_FastElectron:te=0:post:extra=el_tag 
EFelectronTnP:	HLT_e26_lhtight_e14_idperf_tight_nogsf_probe_50invmAB130_L1eEM26M:key=HLT_IDTrack_Electron_FTF:roi=HLT_Roi_FastElectron:te=1:post:extra=el_probe
EFelectronTnP:	HLT_e26_lhtight_e14_idperf_tight_nogsf_probe_50invmAB130_L1eEM26M:key=HLT_IDTrack_Electron_IDTrig:te=0:post:extra=el1_tag 
EFelectronTnP:	HLT_e26_lhtight_e14_idperf_tight_nogsf_probe_50invmAB130_L1eEM26M:key=HLT_IDTrack_Electron_IDTrig:te=1:post:extra=el1_probe
EFelectronTnP:	HLT_e26_lhtight_e14_idperf_tight_probe_50invmAB130_L1eEM26M:key=HLT_IDTrack_Electron_GSF:te=0:post:extra=el2_tag
EFelectronTnP:	HLT_e26_lhtight_e14_idperf_tight_probe_50invmAB130_L1eEM26M:key=HLT_IDTrack_Electron_GSF:te=1:post:extra=el2_probe



L2electronLRT:	HLT_e20_idperf_loose_lrtloose_L1eEM18L:key=HLT_IDTrack_ElecLRT_FTF:roi=HLT_Roi_FastElectron_LRT
L2electronLRT:	HLT_e30_idperf_loose_lrtloose_L1eEM26M:key=HLT_IDTrack_ElecLRT_FTF:roi=HLT_Roi_FastElectron_LRT
L2electronLRT:	HLT_e5_idperf_loose_lrtloose_probe_g25_medium_L1eEM24L:key=HLT_IDTrack_ElecLRT_FTF:roi=HLT_Roi_FastElectron_LRT:0
L2electronLRT:	HLT_e30_lhloose_nopix_lrtmedium_probe_g25_medium_L1eEM24L:key=HLT_IDTrack_ElecLRT_FTF:roi=HLT_Roi_FastElectron_LRT:0
L2electronLRT:	HLT_e26_lhtight_ivarloose_e5_idperf_loose_lrtloose_probe_L1eEM26M:key=HLT_IDTrack_ElecLRT_FTF:roi=HLT_Roi_FastElectron_LRT:1
L2electronLRT:	HLT_e26_lhtight_ivarloose_e30_lhloose_nopix_lrtmedium_probe_L1eEM26M:key=HLT_IDTrack_ElecLRT_FTF:roi=HLT_Roi_FastElectron_LRT:1
    
    
EFelectronLRT:	HLT_e20_idperf_loose_lrtloose_L1eEM18L:key=HLT_IDTrack_ElecLRT_IDTrig:roi=HLT_Roi_FastElectron_LRT
EFelectronLRT:	HLT_e30_idperf_loose_lrtloose_L1eEM26M:key=HLT_IDTrack_ElecLRT_IDTrig:roi=HLT_Roi_FastElectron_LRT
EFelectronLRT:	HLT_e5_idperf_loose_lrtloose_probe_g25_medium_L1eEM24L:key=HLT_IDTrack_ElecLRT_IDTrig:roi=HLT_Roi_FastElectron_LRT:0
EFelectronLRT:	HLT_e30_lhloose_nopix_lrtmedium_probe_g25_medium_L1eEM24L:key=HLT_IDTrack_ElecLRT_IDTrig:roi=HLT_Roi_FastElectron_LRT:0
EFelectronLRT:	HLT_e26_lhtight_ivarloose_e30_lhloose_nopix_lrtmedium_probe_L1eEM26M:key=HLT_IDTrack_ElecLRT_FTF:roi=HLT_Roi_FastElectron_LRT:1
EFelectronLRT:	HLT_e26_lhtight_ivarloose_e30_lhloose_nopix_lrtmedium_probe_L1eEM26M:key=HLT_IDTrack_ElecLRT_IDTrig:roi=HLT_Roi_FastElectron_LRT:1
    


    
L2tau:		HLT_tau25_idperf_tracktwoMVA_L1cTAU20M:key=HLT_IDTrack_TauCore_FTF:roi=HLT_Roi_TauCore 
L2tau:		HLT_tau25_idperf_tracktwoMVA_L1cTAU20M:key=HLT_IDTrack_TauIso_FTF:roi=HLT_Roi_TauIso
    
EFtau:		HLT_tau25_idperf_tracktwoMVA_L1cTAU20M:key=HLT_IDTrack_TauIso_FTF:roi=HLT_Roi_TauIso 
EFtau:		HLT_tau25_idperf_tracktwoMVA_L1cTAU20M:key=HLT_IDTrack_Tau_IDTrig:roi=HLT_Roi_TauIso
    
EFtauvtx:       HLT_tau25_idperf_tracktwoMVA_L1cTAU20M:key=HLT_IDTrack_Tau_IDTrig:roi=HLT_Roi_TauIso:HLT_IDVertex_Tau/HLT_IDVertex_Tau:post:rvtx=HLT_IDVertex_Tau


    
L2tauLRT:     HLT_tau25_idperf_tracktwoLLP_L1cTAU20M:key=HLT_IDTrack_TauCore_FTF:roi=HLT_Roi_TauCore 
L2tauLRT:     HLT_tau25_idperf_tracktwoLLP_L1cTAU20M:key=HLT_IDTrack_TauIso_FTF:roi=HLT_Roi_TauIso 
L2tauLRT:     HLT_tau25_idperf_trackLRT_L1cTAU20M:key=HLT_IDTrack_TauLRT_FTF:roi=HLT_Roi_TauLRT
    
EFtauLRT:      HLT_tau25_idperf_trackLRT_L1cTAU20M:key=HLT_IDTrack_TauLRT_FTF:roi=HLT_Roi_TauLRT 
EFtauLRT:      HLT_tau25_idperf_trackLRT_L1cTAU20M:key=HLT_IDTrack_TauLRT_IDTrig:roi=HLT_Roi_TauLRT 
EFtauLRT:      HLT_tau25_idperf_tracktwoLLP_L1cTAU20M:key=HLT_IDTrack_TauIso_FTF:roi=HLT_Roi_TauIso 
EFtauLRT:      HLT_tau25_idperf_tracktwoLLP_L1cTAU20M:key=HLT_IDTrack_Tau_IDTrig:roi=HLT_Roi_TauIso
    

    
L2bjet:	HLT_j45_0eta290_020jvt_boffperf_pf_ftf_L1jJ50:key=HLT_IDTrack_Bjet_FTF:roi=HLT_Roi_Bjet
L2bjet: HLT_j45_0eta290_020jvt_boffperf_pf_ftf_L1jJ50:key=HLT_IDTrack_FS_FTF:roi=HLT_FSRoI:vtx=HLT_IDVertex_FS
L2bjet: HLT_j80_0eta290_020jvt_boffperf_pf_ftf_L1jJ90:key=HLT_IDTrack_FS_FTF:roi=HLT_FSRoI:vtx=HLT_IDVertex_FS
L2bjet:	HLT_j80_0eta290_020jvt_boffperf_pf_ftf_L1jJ90:key=HLT_IDTrack_JetSuper_FTF:roi=HLT_Roi_JetSuper 
L2bjet:	HLT_j20_roiftf_preselj20_L1RD0_FILLED:HLT:key=IDTrack_JetSuper_FTF:roi=HLT_Roi_JetSuper
        
EFbjet:	HLT_j45_0eta290_020jvt_boffperf_pf_ftf_L1jJ50:key=HLT_IDTrack_Bjet_FTF:roi=HLT_Roi_Bjet 
EFbjet:	HLT_j45_0eta290_020jvt_boffperf_pf_ftf_L1jJ50:key=HLT_IDTrack_Bjet_IDTrig:roi=HLT_Roi_Bjet
    
    

L2fsjet:	HLT_j45_pf_ftf_preselj20_L1jJ40:key=HLT_IDTrack_FS_FTF:roi=HLT_FSRoI:vtx=HLT_IDVertex_FS 
L2fsjet:	HLT_j45_0eta290_020jvt_boffperf_pf_ftf_L1jJ50:key=HLT_IDTrack_FS_FTF:roi=HLT_FSRoI:vtx=HLT_IDVertex_FS
L2fsjet:        HLT_j80_0eta290_020jvt_boffperf_pf_ftf_L1jJ90:key=HLT_IDTrack_FS_FTF:roi=HLT_FSRoI:vtx=HLT_IDVertex_FS
        
L2fsjetvtx:	HLT_j45_pf_ftf_preselj20_L1jJ40:key=HLT_IDTrack_FS_FTF:roi=HLT_FSRoI:vtx=HLT_IDVertex_FS/HLT_IDVertex_FS:post:rvtx=HLT_IDVertex_FS  
L2fsjetvtx:	HLT_j45_0eta290_020jvt_boffperf_pf_ftf_L1jJ50:key=HLT_IDTrack_FS_FTF:roi=HLT_FSRoI:vtx=HLT_IDVertex_FS/HLT_IDVertex_FS:post:rvtx=HLT_IDVertex_FS
    
L2fst0vtx:	HLT_j45_pf_ftf_preselj20_L1jJ40:key=HLT_IDTrack_FS_FTF:roi=HLT_FSRoI:vtx=HLT_IDVertex_FS
L2fst0vtx:	HLT_j45_0eta290_020jvt_boffperf_pf_ftf_L1jJ50:key=HLT_IDTrack_FS_FTF:roi=HLT_FSRoI:vtx=HLT_IDVertex_FS
L2fst0vtx:	HLT_j80_0eta290_020jvt_boffperf_pf_ftf_L1jJ90:key=HLT_IDTrack_FS_FTF:roi=HLT_FSRoI:vtx=HLT_IDVertex_FS
L2fst0vtx:	HLT_j20_roiftf_preselj20_L1RD0_FILLED:key=HLT_IDTrack_JetSuper_FTF:roi=HLT_Roi_JetSuper:vtx=HLT_IDVertex_JetSuper
    
    

L2minbias:	HLT_mb_sptrk_L1RD0_FILLED:key=HLT_IDTrack_MinBias_IDTrig
EFminbias:	HLT_mb_sptrk_L1RD0_FILLED:key=HLT_IDTrack_MinBias_IDTrig
    
L2minbiaspix:	HLT_mb_pixsptrk_nototpix20_q2_L1TRT_ZDC_A_C_VjTE10:key=HLT_IDTrack_MinBiasPixel_IDTrig
EFminbiaspix:	HLT_mb_pixsptrk_nototpix20_q2_L1TRT_ZDC_A_C_VjTE10:key=HLT_IDTrack_MinBiasPixel_IDTrig
    
    

    
L2bphys: 	HLT_2mu4_bBmumux_BsmumuPhi_L12MU3V:key=HLT_IDTrack_Bmumux_FTF  
L2bphys: 	HLT_mu11_mu6_bBmumux_Bidperf_L1MU8VF_2MU5VF:key=HLT_IDTrack_Bmumux_FTF
    
EFbphys: 	HLT_2mu4_bBmumux_BsmumuPhi_L12MU3V:key=HLT_IDTrack_Bmumux_FTF  
EFbphys: 	HLT_2mu4_bBmumux_BsmumuPhi_L12MU3V:key=HLT_IDTrack_Bmumux_IDTrig  
EFbphys: 	HLT_mu11_mu6_bBmumux_Bidperf_L1MU8VF_2MU5VF:key=HLT_IDTrack_Bmumux_FTF  
EFbphys: 	HLT_mu11_mu6_bBmumux_Bidperf_L1MU8VF_2MU5VF:key=HLT_IDTrack_Bmumux_IDTrig
    

EFcosmic:	HLT_mu4_cosmic_L1MU3V_EMPTY:key=HLT_IDTrack_Cosmic_IDTrig
    


L2FSLRT: 	HLT_fslrt0_L1jJ160:key=HLT_IDTrack_FS_FTF:roi=HLT_FSRoI
L2FSLRT:	HLT_fslrt0_L1jJ160:key=HLT_IDTrack_FSLRT_FTF:roi=HLT_FSRoI
        
EFFSLRT:	HLT_fslrt0_L1jJ160:key=HLT_IDTrack_FSLRT_FTF:roi=HLT_FSRoI  
EFFSLRT:	HLT_fslrt0_L1jJ160:key=HLT_IDTrack_FSLRT_IDTrig:roi=HLT_FSRoI
    

beamspot:   HLT_beamspot_trkFS_trkfast_BeamSpotPEB_L14jJ50:key=HLT_IDTrack_FTF_Beamspot
beamspot:   HLT_beamspot_trkFS_trkfast_BeamSpotPEB_L1jJ30_VjTE200:key=HLT_IDTrack_Beamspot
beamspot:   HLT_beamspot_trkFS_trkfast_BeamSpotPEB_L1jJ40:key=HLT_IDTrack_FTF_Beamspot
beamspot:   HLT_j0_ftf_beamspotVtx_L1jJ30_VjTE200:key=HLT_IDTrack_FTF_FS
    

